<html>
<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title>${fileDesc}</title>
<style type="text/css">
body {
	background: #636363;
	border: none;
	padding: 0;
}

.img-responsive {
	width: 900px;
}

.fw_navbar {
	position: fixed;
	top: 0px;
	left: 0px;
	width: 100%;
	height: 50px;
	background: #5379bd;
	line-height: 50px;
	background-color: #000;
}

.fw_navbar_item {
	width: 100px;
	height: 50;
	border: none;
	float: left;
	cursor: pointer;
	text-align: center;
}

.fw_navbar_item:hover {
	background-color: #777;
}

a, span {
	text-decoration: none;
	font-weight: bolder;
	font-size: 16px;
}

.ATTACHMENTSLIST {
	display: none;
	position: absolute;
	width: 100%;
	height: auto;
	background-color: #D2CECE;
	top: 50px;
	left: 0px;
	text-align: left;
	padding-left: 50px;
	padding-top: 20px;
	padding-bottom: 20px;
}

.box {
	width: 397px;
	height: 300px;
	margin: 30px auto;
}

.tbox {
	width: 397px;
	height: 49px;
	background: #0096F5;
	margin-top: 200px;
}

.tbox div {
	width: 0px;
	height: 49px;
	background: #C2D7AC;
	text-align: center;
	font-family: Tahoma;
	font-size: 18px;
	line-height: 48px;
}

a {
	color: black;
}
</style>
</head>
<body oncontextmenu=self.event.returnValue=false onselectstart="return false">
	<div id="navbar"class="fw_navbar">
		<div
			style="TEXT-ALIGN: center; WIDTH: auto; FLOAT: left; HEIGHT: 50px">

			<div id="dl_word" class="fw_navbar_item">
				<a target="_blank"
					href="/getfile?fileMD5=${fileMD5}&suffix=.doc"
					style="COLOR: #fff; FONT-WEIGHT: bolder; TEXT-DECORATION: none">下载原文件</a>
			</div>

			<div id="dl_pdf" class="fw_navbar_item"> 
				<a target="_blank"
					href="/getfile?fileMD5=${fileMD5}&suffix=.pdf"
					style="COLOR: #fff; FONT-WEIGHT: bolder; TEXT-DECORATION: none">下载PDF文件</a>
			</div>
			
			 
			<div id="dl_pdf" class="fw_navbar_item"> 
				<a onclick="printpage()" style="COLOR: #fff; FONT-WEIGHT: bolder; TEXT-DECORATION: none">打印</a>
			</div>
			
		</div>
	</div>
	<div style="TEXT-ALIGN: center; PADDING-TOP: 50px">
		<div id="content">

			<div id="progressBody" class="box">
				<div class="tbox">
					<div class="tiao"></div>
				</div>
				<div id="desc_text"
					style="text-align: center; height: 50px; line-height: 50px; color: #FFFFFF;">正在准备</div>
			</div>

			<div id="imgCenter"
				style="width: 100%; height: auto; text-align: center; display: none">
			</div>

		</div>
	</div>
	
<script src="js/jquery.js" type="text/javascript"></script>
<script src="js/LodopFuncs.js" type="text/javascript"></script>

<script>
	$("#navbar").hide();
	$("#show_attlist").mouseover(function() {
		$(".ATTACHMENTSLIST").show();
	});
	$("#show_attlist").mouseout(function() {
		$(".ATTACHMENTSLIST").hide();
	});
	
	/*add——创建tbx下的div加文字和变宽度的方法*/
	function add(i) {
		var tbox = $(".tbox");
		var tiao = $(".tiao");
		tiao.css("width", i + "%").html(i + "%");
	}
	function guid() {
	    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
	        var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
	        return v.toString(16);
	    });
	}
	add(0.1)
	var interval=setInterval(increment,200);
	var filemd5='${fileMD5}';
	function increment()
	{
		$.get("/progress?fileMD5="+filemd5+"&rd="+guid(),function(data,status){
			// 已经完成
			if(data.status==1){
				clearInterval(interval);
				$("#progressBody").hide();
				$("#imgCenter").hide();
				$("#imgCenter").load("/view?fileMD5="+filemd5);
				$("#imgCenter").fadeIn(3000);
				$("#navbar").show();
			}
			//有异常
			if(data.status==-1){
				alert(data.describe);
				clearInterval(interval);
			}
			$('#desc_text').html(data.describe);
			var total=data.total;
			var currently=data.currently;
			var num=(currently/total)*100
			add(num.toFixed(2));
		});
	}
	
	function printpage(){
		var url ="/view?fileMD5="+filemd5;
		var LODOP = getLodop();
		LODOP.ADD_PRINT_URL(0, 0, "100%", "100%", url);
		LODOP.SET_PRINT_MODE("PRINT_PAGE_PERCENT", "Auto-Width");
		LODOP.PREVIEW();
	}
	
	
	
</script>
</body>
</html>