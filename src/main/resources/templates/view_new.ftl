<html>
<head>
<meta charset="utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="viewport" content="width=device-width, initial-scale=1"/>
<title>${fileDesc}</title>
<link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" />
<link href="css/slider.css" rel="stylesheet" />
<style type="text/css">
			body {
				background-color: #757575;
				padding-top: 70px;
				padding-bottom: 30px;
				border: none;
			}
			
			.theme-dropdown .dropdown-menu {
				position: static;
				display: block;
				margin-bottom: 20px;
			}
			
			.theme-showcase>p>.btn {
				margin: 5px 0;
			}
			
			.theme-showcase .navbar .container {
				width: auto;
			}
			
			#loading {
				height: 100%;
				width: 100%;
				position: fixed;
				z-index: 1;
				left: 0px;
				margin-top: 0px;
				top: 0px;
			}
			
			#loading-center {
				width: 100%;
				height: 100%;
				position: relative;
			}
			.loading-center2 {
				position: absolute;
				left: 50%;
				top: 50%;
				height: 80px;
				width: 200px;
				text-align: center;
				margin-top: -80px;
				margin-left: -100px;
			}
			
			#loading-center-absolute {
				margin: 0 auto;
				position: relative;
				/*position: absolute;
				left: 50%;
				top: 50%;*/
				height: 60px;
				width: 60px;
			}
			
			
			
			.object {
				width: 20px;
				height: 20px;
				background-color: #FFF;
				float: left;
				-moz-border-radius: 50% 50% 50% 50%;
				-webkit-border-radius: 50% 50% 50% 50%;
				border-radius: 50% 50% 50% 50%;
				margin-right: 20px;
				margin-bottom: 20px;
			}
			
			.object:nth-child(2n+0) {
				margin-right: 0px;
			}
			
			#object_one {
				-webkit-animation: object_one 1s infinite;
				animation: object_one 1s infinite;
			}
			
			#object_two {
				-webkit-animation: object_two 1s infinite;
				animation: object_two 1s infinite;
			}
			
			#object_three {
				-webkit-animation: object_three 1s infinite;
				animation: object_three 1s infinite;
			}
			
			#object_four {
				-webkit-animation: object_four 1s infinite;
				animation: object_four 1s infinite;
			}
			
			@-webkit-keyframes loading-center-absolute {
				100% {
					-ms-transform: rotate(360deg);
					-webkit-transform: rotate(360deg);
					transform: rotate(360deg);
				}
			}
			
			@keyframes loading-center-absolute {
				100% {
					-ms-transform: rotate(360deg);
					-webkit-transform: rotate(360deg);
					transform: rotate(360deg);
				}
			}
			
			@-webkit-keyframes object_one {
				50% {
					-ms-transform: translate(20px, 20px);
					-webkit-transform: translate(20px, 20px);
					transform: translate(20px, 20px);
				}
			}
			
			@keyframes object_one {
				50% {
					-ms-transform: translate(20px, 20px);
					-webkit-transform: translate(20px, 20px);
					transform: translate(20px, 20px);
				}
			}
			
			@-webkit-keyframes object_two {
				50% {
					-ms-transform: translate(-20px, 20px);
					-webkit-transform: translate(-20px, 20px);
					transform: translate(-20px, 20px);
				}
			}
			
			@keyframes object_two {
				50% {
					-ms-transform: translate(-20px, 20px);
					-webkit-transform: translate(-20px, 20px);
					transform: translate(-20px, 20px);
				}
			}
			
			@-webkit-keyframes object_three {
				50% {
					-ms-transform: translate(20px, -20px);
					-webkit-transform: translate(20px, -20px);
					transform: translate(20px, -20px);
				}
			}
			
			@keyframes object_three {
				50% {
					-ms-transform: translate(20px, -20px);
					-webkit-transform: translate(20px, -20px);
					transform: translate(20px, -20px);
				}
			}
			
			@-webkit-keyframes object_four {
				50% {
					-ms-transform: translate(-20px, -20px);
					-webkit-transform: translate(-20px, -20px);
					transform: translate(-20px, -20px);
				}
			}
			
			@keyframes object_four {
				50% {
					-ms-transform: translate(-20px, -20px);
					-webkit-transform: translate(-20px, -20px);
					transform: translate(-20px, -20px);
				}
			}
			
			#navbar-fixed-top,#showcase,#msgBox{
				display: none;
			}
			a {
				color: black;
				text-decoration: none;
			}
			.img-responsive{
				text-align: center;
			}
</style>
</head>

<body oncontextmenu="self.event.returnValue=false" onselectstart="return false">

	<!-- Fixed navbar -->
	 <nav id="navbar-fixed-top" class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand hidden-md hidden-lg" href="#">在线阅读</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            			<li>
							<a target="_blank" href="/getfile?fileMD5=${fileMD5}&suffix=.doc">下载原文件</a>
						</li>
						<li>
							<a target="_blank" href="/getfile?fileMD5=${fileMD5}&suffix=.pdf">下载PDF文件</a>
						</li>
						<li class="hidden-xs">
							<a  onclick="printpage()"  href="#">打印</a>
						</li>
          </ul>
          <ul class="nav navbar-nav navbar-right hidden-xs">
            <li><a href="#"  data-toggle="modal" data-target="#zoom" >阅读设置</a></li>
          </ul>
        </div>
        
      </div>
    </nav>
    
	<div id="loading">
		<div id="loading-center">
			<div class="loading-center2">
				<div id="loading-center-absolute">
					<div class="object" id="object_one"></div>
					<div class="object" id="object_two"></div>
					<div class="object" id="object_three"></div>
					<div class="object" id="object_four"></div>
				</div>
				<div id="desc_text" style="margin-top: 60px; height: 20px; width: 100%; color: white; font-size: larger;">正在准备</div>
			</div>
		</div>
	</div>

	<div class="container theme-showcase" role="main">
		<div id="imgCenter" >
		</div>

		<div id="msgBox" class="jumbotron"
			style="color: red; text-align: center; font-size: x-large;">
			对不起！文档打开失败了。</div>
	</div>
	

<div class="modal fade" id="zoom" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
      	<ul class="list-group">
      	  <li class="list-group-item">
      		缩放调整
		  </li>
		  <li class="list-group-item">
		  		<input onChiji="chiji"  placeholder="0 - 100" type="number" min="0" max="100"   step="1" data-color="#1C8EC5" class="srs"></input>
		  </li>
		  <li class="list-group-item">
      		阅读背景色
		  </li>
		  <li class="list-group-item">
				<div class="btn-group btn-group-justified" role="group" >
				  <div class="btn-group" role="group">
				    <button id="setbg01" type="button" style="background-color:#757575;" class="btn btn-default">默认</button>
				  </div>
				  <div class="btn-group" role="group">
				    <button id="setbg02" type="button" style="background-color:#D9EDF7;" class="btn btn-default">淡蓝</button>
				  </div>
				  <div class="btn-group" role="group">
				    <button id="setbg03" type="button" style="background-color:#BFD0B8;" class="btn btn-default">水绿</button>
				  </div>
				  <div class="btn-group" role="group">
				    <button id="setbg04" type="button" style="background-color:#F2DEDE;" class="btn btn-default">粉红</button>
				  </div>
				</div>
		  </li>
		</ul>
      
      
      </div>
      
    </div>
  </div>
</div>


	

<script src="js/jquery.min.js" type="text/javascript"></script>
<script src="js/bootstrap.min.js" type="text/javascript"></script>
<script src="js/LodopFuncs.js" type="text/javascript"></script>
<script src="js/slider.js" type="text/javascript"></script>
<script src="js/sweep.min.js" type="text/javascript"></script>
<script type="text/javascript">
setBgcolor();
//设置背景色
$("#setbg01").click(function(){
	$('body').css("background-color","#757575");
	localStorage.setItem("bgcolor", "#757575");
	});
$("#setbg02").click(function(){
	$('body').css("background-color","#D9EDF7");
	localStorage.setItem("bgcolor", "#D9EDF7");
	});
$("#setbg03").click(function(){
	$('body').css("background-color","#BFD0B8");
	localStorage.setItem("bgcolor", "#BFD0B8");
	});
$("#setbg04").click(function(){
	$('body').css("background-color","#F2DEDE");
	localStorage.setItem("bgcolor", "#F2DEDE");
	});

//设置背景颜色
function setBgcolor(){
	var bgcolor = localStorage.getItem("bgcolor");
	$('body').css("background-color",bgcolor);
	if(bgcolor!='#757575' && bgcolor!=null){
		$('.object').css("background-color","#D82626");
		$('#desc_text').css("color","#222222");
	}
}


//设置阅读配置
function setScale(){
	var scale = localStorage.getItem("scale");
	if(scale==null){
		scale=95
	}
	$(".img-responsive").css("width",scale+"%");
	$(".img-responsive").css("margin"," 0 auto");
	
}


function chiji(i){
	$(".img-responsive").css("width",i+"%");
	$(".img-responsive").css("margin"," 0 auto");
	localStorage.setItem("scale",i);
	//$.get("setScale?scale="+i,function(data,status){});
}


/*add——创建tbx下的div加文字和变宽度的方法*/
function guid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
        return v.toString(16);
    });
}
var interval=setInterval(increment,200);
var filemd5='${fileMD5}';
function increment()
{
	$.get("/progress?fileMD5="+filemd5+"&rd="+guid(),function(data,status){
		// 已经完成
		if(data.status==1){
			clearInterval(interval);
			$('#loading').fadeOut();
			$('#navbar-fixed-top').fadeIn(2000);
			$('#showcase').show();
			$("#imgCenter").hide();
			$("#imgCenter").load("/view?fileMD5="+filemd5);
			$("#imgCenter").fadeIn(2800);
			setTimeout(setScale,200);
		}
		//有异常
		if(data.status==-1){
			alert(data.describe);
			clearInterval(interval);
			$('#loading').hide();
			alert("打开文档失败")
		}
		$('#desc_text').html(data.describe);
	});
}


function printpage(){
	var url ="/view?fileMD5="+filemd5;
	LODOP = getLodop();
	LODOP.SET_PRINT_MODE("NOCLEAR_AFTER_PRINT",true);
	LODOP.ADD_PRINT_URL(0, 0, "100%", "100%", url);
	LODOP.SET_PREVIEW_WINDOW(0, 0, 0, 0, 0, "");
	LODOP.SET_PRINT_MODE("PRINT_PAGE_PERCENT", "Auto-Width");
	LODOP.PREVIEW();
}
</script>
</body>
</html>