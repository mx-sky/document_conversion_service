package com.ly.oa.doc.service.modules.doc.entity;

import java.io.Serializable;

/**
 * 
 * Class Name: CachingFile  
 * Description: 文件数据实体类
 * @author: Liyewang
 * @mail: Liyewang@ly-sky.com 
 * @date: 2018年2月7日
 * @version: 1.0
 *
 */
public class CachingFile implements Serializable {
	private static final long serialVersionUID = 1L;
	String fileMD5;// 文件MD5为主键
	String fileName;// 随机给文件起的一个UUID值，为文件名，后缀
	String fileDesc;// 文件描述，既文件名
	String filePath;// 文件保存在这服务器的路径
	String fileURL;// 文件下载的地址
	long createTime;// 创建时间
	String fileType;// 文件类型
	String otherMsg;// 尾巴，根据这特征能找到文件，有特殊用处，这里的信息用户请求时自定义指定。

	public String getFileMD5() {
		return fileMD5;
	}

	public void setFileMD5(String fileMD5) {
		this.fileMD5 = fileMD5;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileDesc() {
		return fileDesc;
	}

	public void setFileDesc(String fileDesc) {
		this.fileDesc = fileDesc;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getFileURL() {
		return fileURL;
	}

	public void setFileURL(String fileURL) {
		this.fileURL = fileURL;
	}

	public long getCreateTime() {
		return createTime;
	}

	public void setCreateTime(long createTime) {
		this.createTime = createTime;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public String getOtherMsg() {
		return otherMsg;
	}

	public void setOtherMsg(String otherMsg) {
		this.otherMsg = otherMsg;
	}

}
