package com.ly.oa.doc.service.modules.doc.controller;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ly.oa.doc.service.common.utils.RedisUtils;
import com.ly.oa.doc.service.modules.doc.entity.CachingFile;
import com.ly.oa.doc.service.modules.doc.entity.WorkProgressBody;
import com.ly.oa.doc.service.modules.doc.service.DocParseService;
import com.ly.oa.doc.service.modules.doc.service.Pdf2ImageService;
import com.ly.oa.doc.service.modules.doc.utils.BrowserUtils;
import com.ly.oa.doc.service.modules.doc.utils.SimpleThreadLocal;

@Controller
public class ViewDocsController {


	@Autowired
	DocParseService docParseService;
	@Autowired
	Pdf2ImageService pdfConvertImageServiceImpl;
	@Autowired
	private RedisUtils redisUtils;

	protected static Logger logger = LoggerFactory.getLogger(ViewDocsController.class);

	/**
	 * 
	 * Method Name: work Description:
	 * 
	 * @param fileDesc
	 *            文件描述，既文件名
	 * @param fileURL
	 *            文件下载的地址
	 * @param otherMsg
	 *            尾巴，根据这特征能找到文件，有特殊用处，这里的信息用户请求时自定义指定,直接返回去页面
	 * @param model
	 * @param request
	 * @return
	 * @return String
	 * @exception @author
	 *                Liyewang
	 * @mail Liyewang@ly-sky.com
	 * @date: 2018年1月11日
	 */

	@RequestMapping("/convert")
	public String work(String fileDesc, String fileURL, Model model, HttpServletRequest request) {
		System.out.println(fileURL);
		try {
			// 获取文件MD5
			if (fileURL == null || fileURL.equals("")) {
				throw new NullPointerException("没有可浏览的文件");
			} else {
				fileURL = URLDecoder.decode(URLDecoder.decode(fileURL, "UTF-8"), "UTF-8");
			}

			if (fileDesc != null) {
				fileDesc = URLDecoder.decode(URLDecoder.decode(fileDesc, "UTF-8"), "UTF-8");
			}
			String fileMD5 = docParseService.getFileMD5(fileURL);
			model.addAttribute("fileMD5", fileMD5);
			model.addAttribute("fileDesc", fileDesc);
			// 判断用户浏览器
			String reqBrowser = request.getHeader("User-Agent");
			// 判断文件类型
			CachingFile cachingFile = redisUtils.get("dl_" + fileMD5, CachingFile.class);
			String suffix = cachingFile.getFileType();
			suffix = suffix.toLowerCase();
			if (".doc.docx.ppt.pptx.xls.xlsx.txt".contains(suffix)) {
				new Thread(new Runnable() {
					@Override
					public void run() {
						try {
							SimpleThreadLocal.set(fileMD5);
							docParseService.parseOffice();
						} catch (Exception e) {
						}
					}
				}).start();
				if (BrowserUtils.isIE(reqBrowser)) {
					return "view_old";
				}
				return "view_new";
			} else if (suffix.equals(".pdf")) {
				new Thread(new Runnable() {
					@Override
					public void run() {
						try {
							SimpleThreadLocal.set(fileMD5);
							docParseService.parsePDF();
						} catch (Exception e) {
						}
					}
				}).start();
				if (BrowserUtils.isIE(reqBrowser)) {
					return "view_old";
				}
				return "view_new";
			}else {
				throw new Exception("不支持查看该类型的文件：" + suffix);
			}
		} catch (Exception e) {
			model.addAttribute("error","发生了错误");
			logger.equals(e);
			return "error";
		}

	}

	@RequestMapping("/progress")
	public @ResponseBody WorkProgressBody work(WorkProgressBody progress, String fileMD5) {
		if (fileMD5 == null || fileMD5.equals("")) {
			progress.setDescribe("没有MD5值");
			progress.setStatus(-1);
			return progress;
		}
		return docParseService.getWorkProgress(fileMD5);
	}

	@RequestMapping("/view")
	public String view(String fileMD5, Model model) {
		try {
			CachingFile cachingFile = redisUtils.get("ok_" + fileMD5, CachingFile.class);
			model.addAttribute("cachingFile", cachingFile);
			int allSize = pdfConvertImageServiceImpl
					.getPDFSize(cachingFile.getFilePath() + cachingFile.getFileName() + ".pdf");
			List<String> imglist = new ArrayList<String>();
			for (int i = 0; i < allSize; i++) {
				String imgurl = "/getfile?fileMD5=" + fileMD5 + "&allSize=" + allSize + "&no=" + i + "&suffix=.jpg";
				imglist.add(imgurl);
			}
			model.addAttribute("allSize", allSize);
			model.addAttribute("imglist", imglist);
			return "view_img";
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("errorMsg", "加载失败");
			return "error";
		}
	}

	@RequestMapping("/getfile")
	public void view(HttpServletResponse response, String fileMD5, String allSize, String no, String suffix) {
		BufferedInputStream bis = null;
		OutputStream os = null;
		try {
			if (fileMD5 == null || fileMD5.equals("")) {
				throw new NullPointerException("fileMD不能为空");
			}
			CachingFile cachingFile = redisUtils.get("ok_" + fileMD5, CachingFile.class);
			if (cachingFile == null) {
				throw new NullPointerException("找不到记录");
			}

			String fileName = "";
			if (!suffix.equals(".jpg")) {
				fileName = cachingFile.getFilePath() + cachingFile.getFileName() + suffix;
			} else {
				fileName = cachingFile.getFilePath() + cachingFile.getFileName() + "_" + allSize + "_" + no + suffix;
			}
			File file = new File(fileName);
			if (!file.exists()) {
				throw new NullPointerException("找不到文件");
			}
			response.setHeader("content-type", "application/octet-stream");
			response.setContentType("application/octet-stream");
			String filedesc = cachingFile.getFileDesc();
			filedesc = URLEncoder.encode(filedesc, "UTF-8");
			response.setHeader("Content-Disposition", "attachment;filename=" + filedesc + suffix);
			byte[] buff = new byte[1024];
			os = response.getOutputStream();
			bis = new BufferedInputStream(new FileInputStream(file));
			int i = bis.read(buff);
			while (i != -1) {
				os.write(buff, 0, buff.length);
				os.flush();
				i = bis.read(buff);
			}
		} catch (IOException e) {
		} finally {
			if (bis != null) {
				try {
					bis.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

}
