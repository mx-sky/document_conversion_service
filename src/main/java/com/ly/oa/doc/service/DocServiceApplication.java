package com.ly.oa.doc.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class DocServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(DocServiceApplication.class, args);
	}

}
