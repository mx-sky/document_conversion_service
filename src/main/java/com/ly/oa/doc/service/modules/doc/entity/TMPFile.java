package com.ly.oa.doc.service.modules.doc.entity;

public class TMPFile {
	String uuid;
	long createtime;
	String filepath;
	
	

	public TMPFile(String uuid, long createtime, String filepath) {
		super();
		this.uuid = uuid;
		this.createtime = createtime;
		this.filepath = filepath;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public long getCreatetime() {
		return createtime;
	}

	public void setCreatetime(long createtime) {
		this.createtime = createtime;
	}

	public String getFilepath() {
		return filepath;
	}

	public void setFilepath(String filepath) {
		this.filepath = filepath;
	}

	@Override
	public String toString() {
		return "TMPFile [uuid=" + uuid + ", createtime=" + createtime + ", filepath=" + filepath + "]";
	}

}
