package com.ly.oa.doc.service.modules.doc.webservice;

import javax.activation.DataHandler;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "com.ly.oa.doc.service.modules.doc.webservice.Html2PdfService", targetNamespace = "http://webservice.doc.modules.service.doc.oa.ly.com/")
public interface Html2PdfService {

	/**
	 * 
	 * Method Name: HtmlStr2Pdf Description: 把网页URL转成PDF文件
	 * 
	 * @param htmlstr
	 * @return
	 * @return String
	 * @exception @author
	 *                Liyewang
	 * @mail Liyewang@ly-sky.com
	 * @date: 2018年4月12日
	 */
	@WebMethod
	public DataHandler htmlStr2Pdf(@WebParam(name = "htmlstr") String htmlstr);

	/**
	 * 
	 * Method Name: HtmlUrl2Pdf Description: 把网页字符串转成PDF文件
	 * 
	 * @param htmlurl
	 * @return
	 * @return String
	 * @exception @author
	 *                Liyewang
	 * @mail Liyewang@ly-sky.com
	 * @date: 2018年4月12日
	 */
	@WebMethod
	public DataHandler htmlUrl2Pdf(@WebParam(name = "htmlurl") String htmlurl);

	/**
	 * 
	 * Method Name: HtmlStr2Pdf Description: 把网页URL转成图片
	 * 
	 * @param htmlstr
	 * @return
	 * @return String
	 * @exception @author
	 *                Liyewang
	 * @mail Liyewang@ly-sky.com
	 * @date: 2018年4月12日
	 */
	@WebMethod
	public DataHandler htmlStr2IMG(@WebParam(name = "htmlstr") String htmlstr);

	/**
	 * 
	 * Method Name: HtmlUrl2Pdf Description: 把网页字符串转成图片
	 * 
	 * @param htmlurl
	 * @return
	 * @return String
	 * @exception @author
	 *                Liyewang
	 * @mail Liyewang@ly-sky.com
	 * @date: 2018年4月12日
	 */
	@WebMethod
	public DataHandler htmlUrl2IMG(@WebParam(name = "htmlurl") String htmlurl);

}
