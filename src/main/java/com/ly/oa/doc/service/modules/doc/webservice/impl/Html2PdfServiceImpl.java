package com.ly.oa.doc.service.modules.doc.webservice.impl;

import java.io.File;
import java.util.UUID;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ly.oa.doc.service.modules.doc.service.FileService;
import com.ly.oa.doc.service.modules.doc.utils.WKHtmlToPdfUtil;
import com.ly.oa.doc.service.modules.doc.webservice.Html2PdfService;

@Service
public class Html2PdfServiceImpl implements Html2PdfService {
	@Autowired
	FileService fileService;
	static final String workPath = System.getProperty("user.dir") + File.separator + "tmpfile" + File.separator;
	static {
		String workPath = System.getProperty("user.dir") + File.separator + "tmpfile" + File.separator;
		File file = new File(workPath);
		if (!file.exists()) {
			file.mkdirs();
		}
	}

	@Override
	public DataHandler htmlStr2Pdf(String htmlstr) {
		String destPath = workPath + UUID.randomUUID().toString() + ".pdf";
		WKHtmlToPdfUtil.htmlStr2PDF(htmlstr, destPath);
		fileService.addTmpFile(destPath);//让文件服务定时删除这个文件
		DataSource source = new FileDataSource(new File(destPath));
		return new DataHandler(source);
	}

	@Override
	public DataHandler htmlUrl2Pdf(String htmlurl) {
		String destPath = workPath + UUID.randomUUID().toString() + ".pdf";
		WKHtmlToPdfUtil.htmlUrl2PDF(htmlurl, destPath);
		fileService.addTmpFile(destPath);//让文件服务定时删除这个文件
		DataSource source = new FileDataSource(new File(destPath));
		return new DataHandler(source);
	}

	@Override
	public DataHandler htmlStr2IMG(String htmlstr) {
		String destPath = workPath + UUID.randomUUID().toString() + ".jpg";
		WKHtmlToPdfUtil.htmlStr2IMG(htmlstr, destPath);
		fileService.addTmpFile(destPath);//让文件服务定时删除这个文件
		DataSource source = new FileDataSource(new File(destPath));
		return new DataHandler(source);
	}

	@Override
	public DataHandler htmlUrl2IMG(String htmlurl) {
		String destPath = workPath + UUID.randomUUID().toString() + ".jpg";
		WKHtmlToPdfUtil.htmlUrl2IMG(htmlurl, destPath);
		fileService.addTmpFile(destPath);//让文件服务定时删除这个文件
		DataSource source = new FileDataSource(new File(destPath));
		return new DataHandler(source);
	}

}
