package com.ly.oa.doc.service.modules.doc.service;

public interface OfficeConvertService {


	/**
	 * Word文档转换
	 * 
	 * @param inputFile
	 * @param pdfFile
	 * @author SHANHY
	 * @throws Exception
	 */
	public void word2PDF(String inputFile, String pdfFile) throws Exception;

	/**
	 * PPT文档转换
	 * 
	 * @param inputFile
	 * @param pdfFile
	 * @author SHANHY
	 * @throws Exception
	 */
	public void ppt2PDF(String inputFile, String pdfFile) throws Exception;

	/**
	 * Excel文档转换
	 * 
	 * @param inputFile
	 * @param pdfFile
	 * @author SHANHY
	 * @throws Exception
	 */
	public void excel2PDF(String inputFile, String pdfFile) throws Exception;

	public void convert2PDF(String string, String pdfFile) throws Exception;
}
