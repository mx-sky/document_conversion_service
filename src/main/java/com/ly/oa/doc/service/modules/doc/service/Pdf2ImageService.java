package com.ly.oa.doc.service.modules.doc.service;

import java.io.IOException;

public interface Pdf2ImageService {

	public void Pdf2Image(String pdfFile) throws Exception;

	public int getPDFSize(String pdfFile) throws IOException;

}
