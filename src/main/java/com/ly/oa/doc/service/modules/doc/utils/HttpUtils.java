package com.ly.oa.doc.service.modules.doc.utils;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLDecoder;
import java.util.UUID;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class HttpUtils {

	/**
	 * 
	 * Method Name: getFile Description:
	 * 
	 * @param url
	 * @param savePath
	 * @param saveAsName
	 * @return
	 * @throws Exception
	 * @return String 返回的是下载的文件名的后缀
	 * @exception @author
	 *                Liyewang
	 * @mail Liyewang@ly-sky.com
	 * @date: 2018年4月15日
	 */
	public static String getFile(String url, String savePath, String saveAsName) throws Exception {
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		InputStream inputStream = null;
		OutputStream outputStream = null;
		try {
			ResponseEntity<byte[]> response = restTemplate.exchange(url, HttpMethod.GET,
					new HttpEntity<byte[]>(headers), byte[].class);
			String netfilename = response.getHeaders().get("Content-Disposition").get(0);
			netfilename=netfilename.replaceAll("\"","");
			String sufix = netfilename.substring(netfilename.lastIndexOf("."), netfilename.length());
			byte[] result = response.getBody();

			inputStream = new ByteArrayInputStream(result);
			File file=new File(savePath+saveAsName+sufix);
			if(!file.getParentFile().exists()) {
				file.getParentFile().mkdirs();
			}
			if(!file.exists()) {
				file.createNewFile();
			}
			outputStream = new FileOutputStream(file);
			int len = 0;
			byte[] buf = new byte[1024];
			while ((len = inputStream.read(buf, 0, 1024)) != -1) {
				outputStream.write(buf, 0, len);
			}
			outputStream.flush();
			netfilename=netfilename.substring(netfilename.lastIndexOf("=")+1,netfilename.length());
			netfilename=URLDecoder.decode(netfilename, "UTF-8");
			return netfilename;
		} finally {
			if (inputStream != null) {
				inputStream.close();
			}
			if (outputStream != null) {
				outputStream.close();
			}
		}
	}

	public static void main(String[] args) throws Exception {
		String fileSavePath = System.getProperty("user.dir") + File.separator + "workSpace" + File.separator;
		String fileUUID = UUID.randomUUID().toString().replaceAll("-", "");// 文件名，不带后缀
		String res = getFile("http://1.51.29.8:8080/getfile?fileMD5=1e55ca65429198028c90aad4b40adb2b&suffix=.doc",
				fileSavePath, fileUUID);
		System.out.println(res);
	}

}
