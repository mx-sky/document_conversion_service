package com.ly.oa.doc.service.modules.doc.service;

import com.ly.oa.doc.service.modules.doc.entity.WorkProgressBody;

public interface DocParseService {

	/**
	 * 
	 * Method Name:  parseOffice
	 * Description:  把文档转成图片
	 * @throws Exception 
	 * @return void
	 * @exception 	
	 * @author Liyewang
	 * @mail Liyewang@ly-sky.com
	 * @date: 2018年2月1日
	 */
	public void parseOffice() throws Exception;

	/**
	 * 
	 * Method Name:  parsePDF
	 * Description:  把pdf转成图片
	 * @throws Exception 
	 * @return void
	 * @exception 	
	 * @author Liyewang
	 * @mail Liyewang@ly-sky.com
	 * @date: 2018年2月1日
	 */
	public void parsePDF() throws Exception;


	

	/**
	 * (non-Javadoc)
	 * Title: getWorkProgress
	 * Description: 获取进度信息
	 * Created On: 2018年2月1日
	 * @author Liyewang
	 * <p>
	 * @param fileMD5
	 * @return  DocParseServiceImpl
	 * @see com.ly.oa.officeservice.service.DocParseService#getWorkProgress(java.lang.String)
	 */
	public WorkProgressBody getWorkProgress(String fileMD5);
	
	

	/**
	 * (non-Javadoc)
	 * Title: getFileMD5
	 * Description: 下载文件并且获得文件的MD5
	 * Created On: 2018年2月1日
	 * @author Liyewang
	 * <p>
	 * @param strUrl
	 * @return
	 * @throws Exception  DocParseServiceImpl
	 * @see com.ly.oa.officeservice.service.DocParseService#getFileMD5(java.lang.String)
	 */
	public String getFileMD5(String strUrl) throws Exception;


}
