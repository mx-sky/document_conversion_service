package com.ly.oa.doc.service.modules.doc.utils;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * 
 * Class Name: ClearBufferThread  
 * Description: 清理输入流缓存的线程
 * @author: Liyewang
 * @mail: Liyewang@ly-sky.com 
 * @date: 2018年4月12日
 * @version: 1.0
 *
 */
public class ClearBufferThread implements Runnable {
    private InputStream inputStream;
 
    public ClearBufferThread(InputStream inputStream){
        this.inputStream = inputStream;
    }
 
    public void run() {
        try{
            BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
            while(br.readLine() != null);
        } catch(Exception e){
            throw new RuntimeException(e);
        }
    }
}