package com.ly.oa.doc.service.modules.doc.service.impl;

import java.io.File;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ly.oa.doc.service.common.utils.RedisUtils;
import com.ly.oa.doc.service.modules.doc.entity.CachingFile;
import com.ly.oa.doc.service.modules.doc.entity.TMPFile;
import com.ly.oa.doc.service.modules.doc.service.FileService;
import com.ly.oa.doc.service.modules.doc.service.Pdf2ImageService;

/**
 * 
 * Class Name: FileServiceImpl Description: 定时去删除就文件
 * 
 * @author: Liyewang
 * @mail: Liyewang@ly-sky.com
 * @date: 2018年2月1日
 * @version: 1.0
 *
 */
@Service
public class FileServiceImpl implements FileService {
	@Autowired
	private RedisUtils redisUtils;
	
	@Autowired
	Pdf2ImageService pdfConvertImageServiceImpl;
	
	

	@Value("${File_Effective}")
	Integer effectiveTime;

	private static Logger log = LoggerFactory.getLogger(FileServiceImpl.class);

	@Transactional
	@Scheduled(fixedRate = 60000)
	@Override
	public void delfile() {
		if (effectiveTime == null) {
			effectiveTime = 1000 * 60 * 24 * 15;// 15天的默认值
		}
		Set<Object> keys = redisUtils.getKeys("ok_*");
		Iterator<Object> it = keys.iterator();
		while (it.hasNext()) {
			try {
				String key = (String) it.next();
				String fileMD5 = null;
				CachingFile cachingFile =redisUtils.get(key,CachingFile.class);
				if (cachingFile != null) {
					fileMD5 = cachingFile.getFileMD5();
					long now = new Date().getTime();
					if (cachingFile.getCreateTime() + effectiveTime < now) {
						// 需要删除文件
						String filename = cachingFile.getFilePath() + cachingFile.getFileName()
								+ cachingFile.getFileType();
						File file = new File(filename);
						if (file.exists()) {
							file.delete();
							log.info("删除文件：", filename);
						}
						// 删除图片
						int allSize = pdfConvertImageServiceImpl.getPDFSize(cachingFile.getFilePath() + cachingFile.getFileName() + ".pdf");
						for (int no = 0; no < allSize; no++) {
							filename = cachingFile.getFilePath() + cachingFile.getFileName() + "_" + allSize + "_" + no
									+ ".jpg";
							file = new File(filename);
							if (file.exists()) {
								file.delete();
								log.info("删除文件：", filename);
							}
						}
						// 删除PDF
						filename = cachingFile.getFilePath() + cachingFile.getFileName() + ".pdf";
						file = new File(filename);
						if (file.exists()) {
							file.delete();
							log.info("删除文件：", filename);
						}
						// 删除key
						redisUtils.delete(key);
						if (redisUtils.exists("dl_" + fileMD5)) {
							redisUtils.delete("dl_" + fileMD5);
							log.info("删除key：", "dl_" + fileMD5);
						}
					}

				}
			} catch (Exception e) {
				continue;
			}
		}
	}

	@Transactional
	@Scheduled(fixedRate = 60000)
	@Override
	public void delTmpFile() {
		if (effectiveTime == null) {
			System.out.println("effectiveTime为空");
			effectiveTime = 1000 * 60 * 24 * 15;// 15天的默认值
		}
		Set<Object> keys = redisUtils.getKeys("tmp_*");
		Iterator<Object> it = keys.iterator();
		while (it.hasNext()) {
			String key = (String) it.next();
			TMPFile tmpFileObj = redisUtils.get(key, TMPFile.class);
			File tmpFile = new File(tmpFileObj.getFilepath());
			long now = new Date().getTime();
			if (tmpFileObj.getCreatetime() + effectiveTime < now) {
				if (tmpFile.exists()) {
					tmpFile.delete();
				}
			}
		}

	}

	@Override
	public void addTmpFile(String filePath) {
		String uuid = UUID.randomUUID().toString();
		redisUtils.set(uuid, new TMPFile("tmp_" + uuid, new Date().getTime(), filePath));
	}

}
