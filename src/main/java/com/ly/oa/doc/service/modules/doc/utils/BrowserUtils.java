package com.ly.oa.doc.service.modules.doc.utils;

public class BrowserUtils {
	/**
	 * 
	 * Method Name:  isIE
	 * Description:  判断是否是IE浏览器
	 * @param agent
	 * @return 
	 * @return boolean
	 * @exception 	
	 * @author Liyewang
	 * @mail Liyewang@ly-sky.com
	 * @date: 2018年1月12日
	 */
	public static boolean isIE(String agent) {
		if (agent.indexOf("MSIE") > 0) {
			return true;
		}
		return false;
	}
}