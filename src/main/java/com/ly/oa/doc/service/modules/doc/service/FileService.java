package com.ly.oa.doc.service.modules.doc.service;

public interface FileService {
	
	/**
	 * 
	 * Method Name:  delfile
	 * Description:   定时删除老旧的文件
	 * @return void
	 * @exception 	
	 * @author Liyewang
	 * @mail Liyewang@ly-sky.com
	 * @date: 2018年1月12日
	 */
	public void delfile();
	
	/**
	 * 
	 * Method Name:  delTmpFile
	 * Description:   记录临时文件，会被自动删除
	 * @return void
	 * @exception 	
	 * @author Liyewang
	 * @mail Liyewang@ly-sky.com
	 * @date: 2018年4月12日
	 */
	public void delTmpFile();
	
	/**
	 * 
	 * Method Name:  addTmpFile
	 * Description:  添加临时文件，会被自动删除
	 * @param filePath 
	 * @return void
	 * @exception 	
	 * @author Liyewang
	 * @mail Liyewang@ly-sky.com
	 * @date: 2018年4月12日
	 */
	public void addTmpFile(String filePath);
	
}
