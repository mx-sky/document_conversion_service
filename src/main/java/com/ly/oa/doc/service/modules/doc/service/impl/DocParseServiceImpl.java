package com.ly.oa.doc.service.modules.doc.service.impl;

import java.io.File;
import java.util.Date;
import java.util.UUID;

import javax.servlet.ServletContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ly.oa.doc.service.common.utils.RedisUtils;
import com.ly.oa.doc.service.modules.doc.entity.CachingFile;
import com.ly.oa.doc.service.modules.doc.entity.WorkProgressBody;
import com.ly.oa.doc.service.modules.doc.service.DocParseService;
import com.ly.oa.doc.service.modules.doc.service.OfficeConvertService;
import com.ly.oa.doc.service.modules.doc.service.Pdf2ImageService;
import com.ly.oa.doc.service.modules.doc.utils.FileUtils;
import com.ly.oa.doc.service.modules.doc.utils.HttpUtils;
import com.ly.oa.doc.service.modules.doc.utils.SimpleThreadLocal;


@Service
public class DocParseServiceImpl implements DocParseService {
	
	@Autowired
	private RedisUtils redisUtils;

	@Autowired
	FileServiceImpl fileServiceImpl;
	@Autowired
	OfficeConvertService officeConvertServiceImpl;
	@Autowired
	Pdf2ImageService PdfConvertImageServiceImpl;
	@Autowired
	ServletContext context;

	protected static Logger logger = LoggerFactory.getLogger(DocParseServiceImpl.class);
	

	/**
	 * 
	 * Method Name:  parseOffice
	 * Description:  把文档转成图片
	 * @throws Exception 
	 * @return void
	 * @exception 	
	 * @author Liyewang
	 * @mail Liyewang@ly-sky.com
	 * @date: 2018年2月1日
	 */
	public void parseOffice() throws Exception {
		String fileMD5 = (String) SimpleThreadLocal.get();
		boolean success = true;
		// 该MD5文件没有被处理
		if (null != redisUtils.get("working_" + fileMD5)) {
			return;
		}
		if (null == redisUtils.get("ok_" + fileMD5)) {
			CachingFile cachingFile =redisUtils.get("dl_" + fileMD5,CachingFile.class);
			String fileSavePath = null, fileUUID = null, suffix = null, oldFileName = null,pdfFile=null;
			fileSavePath = cachingFile.getFilePath();
			fileUUID = cachingFile.getFileName();
			suffix = cachingFile.getFileType();
			oldFileName = cachingFile.getFileDesc();
			
			//==========================================
			WorkProgressBody progress = new WorkProgressBody();
			progress.setDescribe("正在处理文件");
			redisUtils.set("working_" + fileMD5, progress);
			//==========================================
			
			try {
				logger.info("开始转换...");
				pdfFile = fileSavePath + fileUUID + ".pdf";
				logger.info("正在把文件转为PDF...");
				officeConvertServiceImpl.convert2PDF(fileSavePath + fileUUID + suffix, pdfFile);
				logger.info("正在把PDF转为图片...");
				PdfConvertImageServiceImpl.Pdf2Image(pdfFile);
				logger.info("结束转换...");
				
				
				
				//==========================================
				progress.setDescribe("已就绪");
				progress.setStatus(1);
				redisUtils.set("working_" + fileMD5, progress);
				//==========================================
				
				
			} catch (Exception e) {
				//==========================================
				progress.setDescribe("加载失败");
				progress.setStatus(-1);
				redisUtils.set("working_" + fileMD5, progress);
				//==========================================
				success = false;
				throw new Exception("转换时发生异常");
			} finally {
				redisUtils.delete("working_" + fileMD5);
				if (success) {
					// 添加记录到历史转换缓存
					CachingFile CachingFile = new CachingFile();
					CachingFile.setCreateTime(new Date().getTime());
					CachingFile.setFileMD5(fileMD5);
					CachingFile.setFileName(fileUUID);
					CachingFile.setFilePath(fileSavePath);
					CachingFile.setFileType(suffix);
					oldFileName = oldFileName.substring(0, oldFileName.lastIndexOf('.'));
					CachingFile.setFileDesc(oldFileName);
					redisUtils.set("ok_" + fileMD5, CachingFile);
				}
			}
		}
	}

	/**
	 * 
	 * Method Name:  parsePDF
	 * Description:  把pdf转成图片
	 * @throws Exception 
	 * @return void
	 * @exception 	
	 * @author Liyewang
	 * @mail Liyewang@ly-sky.com
	 * @date: 2018年2月1日
	 */
	public void parsePDF() throws Exception{
		String fileMD5 = (String) SimpleThreadLocal.get();
		boolean success = true;
		// 该MD5文件没有被处理
		if (null != redisUtils.get("working_" + fileMD5)) {
			return;
		}
		if (null == redisUtils.get("ok_" + fileMD5)) {
			CachingFile cachingFile =redisUtils.get("dl_" + fileMD5,CachingFile.class);
			String fileSavePath = null, fileUUID = null, suffix = null, oldFileName = null,pdfFile=null;
			fileSavePath = cachingFile.getFilePath();
			fileUUID = cachingFile.getFileName();
			suffix = cachingFile.getFileType();
			oldFileName = cachingFile.getFileDesc();
			
			//==========================================
			WorkProgressBody progress = new WorkProgressBody();
			progress.setDescribe("正在处理文件");
			redisUtils.set("working_" + fileMD5, progress);
			//==========================================
			
			try {
				logger.info("开始转换...");
				logger.info("正在把PDF转为图片...");
				pdfFile = fileSavePath + fileUUID +suffix;
				PdfConvertImageServiceImpl.Pdf2Image(pdfFile);
				logger.info("结束转换...");
				
				//==========================================
				progress.setDescribe("已就绪");
				progress.setStatus(1);
				redisUtils.set("working_" + fileMD5, progress);
				//==========================================
				
				
			} catch (Exception e) {
				//==========================================
				progress.setDescribe("加载失败");
				progress.setStatus(-1);
				redisUtils.set("working_" + fileMD5, progress);
				//==========================================
				success = false;
				throw new Exception("转换时发生异常");
			} finally {
				redisUtils.delete("working_" + fileMD5);
				if (success) {
					// 添加记录到历史转换缓存
					CachingFile CachingFile = new CachingFile();
					CachingFile.setCreateTime(new Date().getTime());
					CachingFile.setFileMD5(fileMD5);
					CachingFile.setFileName(fileUUID);
					CachingFile.setFilePath(fileSavePath);
					CachingFile.setFileType(suffix);
					oldFileName = oldFileName.substring(0, oldFileName.lastIndexOf('.'));
					CachingFile.setFileDesc(oldFileName);
					redisUtils.set("ok_" + fileMD5, CachingFile);
				}
			}
		}
	}


	

	/**
	 * (non-Javadoc)
	 * Title: getWorkProgress
	 * Description: 获取进度信息
	 * Created On: 2018年2月1日
	 * @author Liyewang
	 * <p>
	 * @param fileMD5
	 * @return  DocParseServiceImpl
	 * @see com.ly.oa.officeservice.service.DocParseService#getWorkProgress(java.lang.String)
	 */
	public WorkProgressBody getWorkProgress(String fileMD5) {
		WorkProgressBody progress2 =redisUtils.get("working_" + fileMD5,WorkProgressBody.class);
		if (progress2 == null) {
			progress2 = new WorkProgressBody();
			if (null == redisUtils.get("ok_" + fileMD5)) {
				progress2.setDescribe("找不到该任务");
				progress2.setStatus(-1);
				return progress2;
			} else {
				progress2.setDescribe("已就绪");
				progress2.setStatus(1);
				return progress2;
			}
		}
		return progress2;
	}
	
	

	/**
	 * (non-Javadoc)
	 * Title: getFileMD5
	 * Description: 下载文件并且获得文件的MD5
	 * Created On: 2018年2月1日
	 * @author Liyewang
	 * <p>
	 * @param strUrl
	 * @return
	 * @throws Exception  DocParseServiceImpl
	 * @see com.ly.oa.officeservice.service.DocParseService#getFileMD5(java.lang.String)
	 */
	public String getFileMD5(String strUrl) throws Exception {
		String fileSavePath = System.getProperty("user.dir") + File.separator + "workSpace" + File.separator;
		String fileUUID = UUID.randomUUID().toString().replaceAll("-", "");// 文件名，不带后缀
		String oldFileName = HttpUtils.getFile(strUrl, fileSavePath, fileUUID);
		String suffix = oldFileName.substring(oldFileName.lastIndexOf('.'), oldFileName.length());
		String fileMD5 = FileUtils.getMD5(fileSavePath + fileUUID + suffix);
		if (null != redisUtils.get("ok_" + fileMD5)) {
			// 以前已经下载好了，并且转换好了,所以删除现在下载的文件
			File nowFile = new File(fileSavePath + fileUUID + suffix);
			if (nowFile.exists()) {
				nowFile.delete();
			}
		} else {
			// 记录下现在下载的这个文件，让定时调度器去删除它
			CachingFile cfile = new CachingFile();
			cfile.setFileMD5(fileMD5);
			cfile.setFileDesc(oldFileName);
			cfile.setFilePath(fileSavePath);
			cfile.setFileURL(strUrl);
			cfile.setFileType(suffix);
			cfile.setFileName(fileUUID);
			redisUtils.set("dl_" + fileMD5, cfile);
		}
		return fileMD5;
	}

}