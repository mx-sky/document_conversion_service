package com.ly.oa.doc.service.common.config;


import javax.xml.ws.Endpoint;

import org.apache.cxf.Bus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.ly.oa.doc.service.modules.doc.webservice.Html2PdfService;

@Configuration
public class CxfConfig {

	@Autowired
	private Bus bus;

	@Autowired
	Html2PdfService html2PdfServiceImpl;

	/** JAX-WS **/
	@Bean
	public Endpoint endpoint() {
		EndpointImpl endpoint = new EndpointImpl(bus, html2PdfServiceImpl);
		endpoint.publish("/html2PdfService");
		return endpoint;
	}
}
