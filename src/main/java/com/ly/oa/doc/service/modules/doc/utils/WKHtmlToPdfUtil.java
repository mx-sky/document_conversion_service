package com.ly.oa.doc.service.modules.doc.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

/**
 * 
 * Class Name: WKHtmlToPdfUtil Description: 把网页转为pdf的工具
 * 
 * @author: Liyewang
 * @mail: Liyewang@ly-sky.com
 * @date: 2018年4月12日
 * @version: 1.0
 *
 */
public class WKHtmlToPdfUtil {
	public static final String WKHTMLTOPDF_PATH = System.getProperty("user.dir") + File.separator + "wkhtmltopdf"
			+ File.separator + "bin" + File.separator;

	/**
	 * 
	 * Method Name: htmlUrl2pdf Description: 把网页转为pdf
	 * 
	 * @param srcPath
	 *            网址或HTML文件路径
	 * @param destPath
	 *            PDF保存路径
	 * @return
	 * @return boolean
	 * @exception @author
	 *                Liyewang
	 * @mail Liyewang@ly-sky.com
	 * @date: 2018年4月12日
	 */
	public static void htmlUrl2PDF(String srcPath, String destPath) {
		File file = new File(destPath);
		File parent = file.getParentFile();
		if (!parent.exists()) {
			parent.mkdirs();
		}
		StringBuilder cmd = new StringBuilder();
		cmd.append(WKHTMLTOPDF_PATH);
		cmd.append("wkhtmltopdf.exe");
		cmd.append("  --header-line");// 页眉下面的线
		// cmd.append(" --header-center 这里是页眉这里是页眉这里是页眉这里是页眉 ");//页眉中间内容
		// cmd.append(" --margin-top 30mm ");//设置页面上边距 (default 10mm)
		cmd.append(" --header-spacing 10 ");// (设置页眉和内容的距离,默认0)
		cmd.append(srcPath);
		cmd.append(" "); // 不能去掉
		cmd.append(destPath);

		try {
			Process process = Runtime.getRuntime().exec(cmd.toString());
			new Thread(new ClearBufferThread(process.getInputStream())).start();
			new Thread(new ClearBufferThread(process.getErrorStream())).start();
			process.waitFor();
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	/**
	 * 
	 * Method Name: htmlUrl2pdf Description: 把网页转为图片
	 * 
	 * @param srcPath
	 *            网址或HTML文件路径
	 * @param destPath
	 *            PDF保存路径
	 * @return
	 * @return boolean
	 * @exception @author
	 *                Liyewang
	 * @mail Liyewang@ly-sky.com
	 * @date: 2018年4月12日
	 */
	public static void htmlUrl2IMG(String srcPath, String destPath) {
		StringBuilder cmd = new StringBuilder();
		cmd.append(WKHTMLTOPDF_PATH);
		cmd.append("wkhtmltoimage.exe");
		cmd.append(" "); // 不能去掉
		cmd.append(srcPath);
		cmd.append(" "); // 不能去掉
		cmd.append(destPath);
		try {
			Process process = Runtime.getRuntime().exec(cmd.toString());
			new Thread(new ClearBufferThread(process.getInputStream())).start();
			new Thread(new ClearBufferThread(process.getErrorStream())).start();
			process.waitFor();
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	
	/**
	 * 
	 * Method Name:  htmlStr2IMG
	 * Description: 将网页字符串转图片 
	 * @param htmstr
	 * @param destPath 
	 * @return void
	 * @exception 	
	 * @author Liyewang
	 * @mail Liyewang@ly-sky.com
	 * @date: 2018年4月12日
	 */
	public static void htmlStr2IMG(String htmstr, String destPath) {
		String tmp_html=destPath.substring(0,destPath.lastIndexOf("."))+".html";
		htmlUrl2IMG(strToHtmlFile(htmstr,tmp_html),destPath);
		File tmpFile=new File(tmp_html);
		if(tmpFile.exists()){
			tmpFile.delete();
		}
	}
	
	/**
	 * 
	 * Method Name:  htmlStr2PDF
	 * Description:  将网页字符串转PDF
	 * @param htmstr
	 * @param destPath 
	 * @return void
	 * @exception 	
	 * @author Liyewang
	 * @mail Liyewang@ly-sky.com
	 * @date: 2018年4月12日
	 */
	public static void htmlStr2PDF(String htmstr, String destPath) {
		String tmp_html=destPath.substring(0,destPath.lastIndexOf("."))+".html";
		htmlUrl2PDF(strToHtmlFile(htmstr,tmp_html),destPath);
		File tmpFile=new File(tmp_html);
		if(tmpFile.exists()){
			tmpFile.delete();
		}
	}
	
	/**
	 * 
	 * Method Name:  strToHtmlFile
	 * Description:  将HTML字符串转换为HTML文件
	 * @param htmlStr
	 * @return 
	 * @return String
	 * @exception 	
	 * @author Liyewang
	 * @mail Liyewang@ly-sky.com
	 * @date: 2018年4月12日
	 */
	public static String strToHtmlFile(String htmlStr,String destPath) {
		OutputStream outputStream = null;
		try {
			outputStream = new FileOutputStream(destPath);
			outputStream.write(htmlStr.getBytes("UTF-8"));
			return destPath;
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			try {
				if (outputStream != null) {
					outputStream.close();
					outputStream = null;
				}
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
	}

	 public static void main(String[] args) {
		 String htmlStr = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\"><html><head><meta http-equiv=\"Content-Type\" content=\"text/html;charset=UTF-8\"></meta><title>HTML转PDF</title></head><body><h1>Hello 世界！</h1></body></html>";
//		 htmlStr2IMG("http://192.168.2.176:8080/bpm/rule?wf_num=R_formprint_B001&wf_docunid=4b785b1208d1c049a00bb3b0723059ccce8a","C:\\aaa.jpg");
//		 htmlStr2IMG(htmlStr,"C:\\aaa.jpg");
		 htmlStr2PDF(htmlStr,"C:\\aaa.pdf");
	 }

}