package com.ly.oa.doc.service.modules.doc.service.impl;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.ImageType;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.ly.oa.doc.service.common.utils.RedisUtils;
import com.ly.oa.doc.service.modules.doc.entity.WorkProgressBody;
import com.ly.oa.doc.service.modules.doc.service.Pdf2ImageService;
import com.ly.oa.doc.service.modules.doc.utils.SimpleThreadLocal;

/**
 * 
 * Class Name: Pdf2ImageServiceImpl Description: 把PDF转图片
 * 
 * @author: Liyewang
 * @mail: Liyewang@ly-sky.com
 * @date: 2018年2月7日
 * @version: 1.0
 *
 */
@Service
public class Pdf2ImageServiceImpl implements Pdf2ImageService {
	@Autowired
	private RedisUtils redisUtils;
	@Value("${IMG_GRADE}")
	Integer imgGrade;

	@Override
	public void Pdf2Image(String pdfFile) {
		if (imgGrade == null) {
			imgGrade = 150;
		}
		InputStream is = null;
		PDDocument pdf = null;
		String filenum = pdfFile.substring(0, pdfFile.lastIndexOf('.'));
		try {
			is = new FileInputStream(pdfFile);
			pdf = PDDocument.load(is);
			int actSize = pdf.getNumberOfPages();

			WorkProgressBody progress = new WorkProgressBody();
			progress.setDescribe("正在渲染");
			progress.setTotal(actSize);
			redisUtils.set("working_" + (String) SimpleThreadLocal.get(), progress);

			for (int i = 0; i < actSize; i++) {
				BufferedImage image = new PDFRenderer(pdf).renderImageWithDPI(i, imgGrade, ImageType.RGB);
				File outFile = new File(filenum + "_" + actSize + "_" + i + ".jpg");
				ImageIO.write(image, "jpg", outFile);
				image = null;
				Runtime.getRuntime().gc();
				double all = actSize;
				double cc = ((i + 1) / all) * 100;

				progress.setCurrently(i + 1);
				progress.setDescribe("正在渲染" + String.format("%.2f", cc) + "%");
				redisUtils.set("working_" + (String) SimpleThreadLocal.get(), progress);

			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				pdf.close();
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public int getPDFSize(String pdfFile) throws IOException {
		InputStream is = null;
		PDDocument pdf = null;
		try {
			is = new FileInputStream(pdfFile);
			pdf = PDDocument.load(is);
			int actSize = pdf.getNumberOfPages();
			return actSize;
		} catch (Exception e) {
			throw e;
		} finally {
			try {
				pdf.close();
				is.close();
			} catch (IOException e) {
				throw e;
			}
		}
	}

}
