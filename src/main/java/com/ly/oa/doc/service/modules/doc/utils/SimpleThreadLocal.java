package com.ly.oa.doc.service.modules.doc.utils;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
/**
 * 
 * Class Name: SimpleThreadLocal  
 * Description: 线程工具类，把变量保存进当前线程中
 * @author: Liyewang
 * @mail: Liyewang@ly-sky.com 
 * @date: 2018年1月12日
 * @version: 1.0
 *
 */
public class SimpleThreadLocal {
	static private Map valueMap = Collections.synchronizedMap(new HashMap());

	public static void set(Object newValue) {
		valueMap.put(Thread.currentThread(), newValue);// ①键为线程对象，值为本线程的变量副本
	}

	public static Object get() {
		Thread currentThread = Thread.currentThread();
		Object o = valueMap.get(currentThread);// ②返回本线程对应的变量
		if (o == null && !valueMap.containsKey(currentThread)) {// ③如果在Map中不存在，放到Map 中保存起来。
			o = initialValue();
			valueMap.put(currentThread, o);
		}
		return o;
	}

	public static void remove() {
		valueMap.remove(Thread.currentThread());
	}

	public static Object initialValue() {
		return null;
	}
}