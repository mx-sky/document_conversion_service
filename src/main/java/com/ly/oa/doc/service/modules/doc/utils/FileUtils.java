package com.ly.oa.doc.service.modules.doc.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.security.MessageDigest;

public class FileUtils {
	
	/**
	 * 
	 * Method Name:  getMD5
	 * Description:  获取文件的MD5
	 * @param filepath
	 * @return
	 * @throws Exception 
	 * @return String
	 * @exception 	
	 * @author Liyewang
	 * @mail Liyewang@ly-sky.com
	 * @date: 2018年1月11日
	 */
	public static String getMD5(String filepath) throws Exception {
		File file = new File(filepath);
		String value = null;
		FileInputStream in = new FileInputStream(file);
		try {
			MappedByteBuffer byteBuffer = in.getChannel().map(FileChannel.MapMode.READ_ONLY, 0, file.length());
			MessageDigest md5 = MessageDigest.getInstance("MD5");
			md5.update(byteBuffer);
			BigInteger bi = new BigInteger(1, md5.digest());
			value = bi.toString(16);
		} catch (Exception e) {
			throw e;
		} finally {
			if (null != in) {
				try {
					in.close();
				} catch (IOException e) {
					throw e;
				}
			}
		}
		return value;
	}
	
	/**
	 * 
	 * Method Name:  getFileSufix
	 * Description:  获取文件后缀
	 * @param fileName
	 * @return 
	 * @return String
	 * @exception 	
	 * @author Liyewang
	 * @mail Liyewang@ly-sky.com
	 * @date: 2018年2月1日
	 */
	public static String getFileSufix(String fileName) {
		int splitIndex = fileName.lastIndexOf(".");
		return fileName.substring(splitIndex + 1);
	}
}
