package com.ly.oa.doc.service.modules.doc.entity;

import java.io.Serializable;

/**
 * 
 * Class Name: WorkProgressBody  
 * Description: 进度条实体类
 * @author: Liyewang
 * @mail: Liyewang@ly-sky.com 
 * @date: 2018年2月7日
 * @version: 1.0
 *
 */
public class WorkProgressBody implements Serializable {
	private static final long serialVersionUID = 1L;
	String describe;// 描述
	int status;// 状态 0：进行中 1：完成 -1:失败
	long total; // 全部任务量
	long currently;// 现在处理进度

	public String getDescribe() {
		return describe;
	}

	public void setDescribe(String describe) {
		this.describe = describe;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public long getTotal() {
		return total;
	}

	public void setTotal(long total) {
		this.total = total;
	}

	public long getCurrently() {
		return currently;
	}

	public void setCurrently(long currently) {
		this.currently = currently;
	}

}
