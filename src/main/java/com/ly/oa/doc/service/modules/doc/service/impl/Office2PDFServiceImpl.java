package com.ly.oa.doc.service.modules.doc.service.impl;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jacob.activeX.ActiveXComponent;
import com.jacob.com.ComThread;
import com.jacob.com.Dispatch;
import com.jacob.com.Variant;
import com.ly.oa.doc.service.common.utils.RedisUtils;
import com.ly.oa.doc.service.modules.doc.entity.WorkProgressBody;
import com.ly.oa.doc.service.modules.doc.service.OfficeConvertService;
import com.ly.oa.doc.service.modules.doc.utils.SimpleThreadLocal;
/**
 * 
 * Class Name: Office2PDFServiceImpl  
 * Description: 把doc,ppt,xls文档转PDF
 * @author: Liyewang
 * @mail: Liyewang@ly-sky.com 
 * @date: 2018年2月7日
 * @version: 1.0
 *
 */
@Service
public class Office2PDFServiceImpl implements OfficeConvertService {
	@Autowired
	private RedisUtils redisUtils;
	
	private static Logger log = LoggerFactory.getLogger(Office2PDFServiceImpl.class);

	/* 转PDF格式值 */
	private static final int wdFormatPDF = 17;
	private static final int xlFormatPDF = 0;
	private static final int ppFormatPDF = 32;
	private static final int msoTrue = -1;
	private static final int msofalse = 0;

	/* 转HTML格式值 */
	private static final int wdFormatHTML = 8;
	private static final int ppFormatHTML = 12;
	private static final int xlFormatHTML = 44;

	/* 转TXT格式值 */
	private static final int wdFormatTXT = 2;

	public void convert2PDF(String inputFile, String pdfFile) throws Exception {
		WorkProgressBody progress=new WorkProgressBody();
		progress.setDescribe("正在处理文件");
		progress.setTotal(100);
		progress.setCurrently(20);;
		redisUtils.set("working_" + (String) SimpleThreadLocal.get(),progress);
		
		
		String suffix = getFileSufix(inputFile);
		File file = new File(inputFile);
		if (!file.exists()) {
			log.error("文件不存在！");
		}
		if (suffix.equals("doc") || suffix.equals("docx") || suffix.equals("txt")) {
			 word2PDF(inputFile, pdfFile);
		} else if (suffix.equals("ppt") || suffix.equals("pptx")) {
			 ppt2PDF(inputFile, pdfFile);
		} else if (suffix.equals("xls") || suffix.equals("xlsx")) {
			 excel2PDF(inputFile, pdfFile);
		} else {
			log.info("文件格式不支持转换!");
			throw new Exception("文件格式不支持转换!");
		}
	}

	/**
	 * 获取文件后缀
	 * 
	 * @param fileName
	 * @return
	 * @author SHANHY
	 */
	public String getFileSufix(String fileName) {
		int splitIndex = fileName.lastIndexOf(".");
		return fileName.substring(splitIndex + 1);
	}

	/**
	 * (non-Javadoc)
	 * Title: word2PDF
	 * Description: Word文档转PDF
	 * Created On: 2018年2月1日
	 * @author Liyewang
	 * <p>
	 * @param inputFile
	 * @param pdfFile  Office2PDFServiceImpl
	 * @throws Exception 
	 * @see com.ly.oa.officeservice.service.OfficeConvertService#word2PDF(java.lang.String, java.lang.String)
	 */
	public void word2PDF(String inputFile, String pdfFile) throws Exception {
		ComThread.InitSTA();

		long start = System.currentTimeMillis();
		ActiveXComponent app = null;
		Dispatch doc = null;
		try {
			app = new ActiveXComponent("Word.Application");// 创建一个word对象
			app.setProperty("Visible", new Variant(false)); // 不可见打开word
			app.setProperty("AutomationSecurity", new Variant(3)); // 禁用宏
			Dispatch docs = app.getProperty("Documents").toDispatch();// 获取文挡属性

			log.info("打开文档 >>> " + inputFile);
			// Object[]第三个参数是表示“是否只读方式打开”
			// 调用Documents对象中Open方法打开文档，并返回打开的文档对象Document
			doc = Dispatch.call(docs, "Open", inputFile, false, true).toDispatch();
			// 调用Document对象的SaveAs方法，将文档保存为pdf格式
			log.info("转换文档 [" + inputFile + "] >>> [" + pdfFile + "]");
			Dispatch.call(doc, "SaveAs", pdfFile, wdFormatPDF);// word保存为pdf格式宏，值为17
			// Dispatch.call(doc, "ExportAsFixedFormat", pdfFile, wdFormatPDF);
			// // word保存为pdf格式宏，值为17
			long end = System.currentTimeMillis();
			log.info("用时：" + (end - start) + "ms.");
		} catch (Exception e) {
			log.error("文档转换失败：" + e);
			throw new Exception("文档转换失败："+e.getMessage());
		} finally {
			Dispatch.call(doc, "Close", false);
			log.info("关闭文档");
			if (app != null)
				app.invoke("Quit", new Variant[] {});
			// 如果没有这句话,winword.exe进程将不会关闭
			ComThread.Release();
			ComThread.quitMainSTA();
		}
	}

	/**
	 * (non-Javadoc)
	 * Title: ppt2PDF
	 * Description: PPT文档转PDF
	 * Created On: 2018年2月1日
	 * @author Liyewang
	 * <p>
	 * @param inputFile
	 * @param pdfFile
	 * @throws Exception  Office2PDFServiceImpl
	 * @see com.ly.oa.officeservice.service.OfficeConvertService#ppt2PDF(java.lang.String, java.lang.String)
	 */
	public void ppt2PDF(String inputFile, String pdfFile) throws Exception {
		ComThread.InitSTA();

		long start = System.currentTimeMillis();
		ActiveXComponent app = null;
		Dispatch ppt = null;
		try {
			app = new ActiveXComponent("PowerPoint.Application");// 创建一个PPT对象
			// app.setProperty("Visible", new Variant(false)); //
			// 不可见打开（PPT转换不运行隐藏，所以这里要注释掉）
			// app.setProperty("AutomationSecurity", new Variant(3)); // 禁用宏
			Dispatch ppts = app.getProperty("Presentations").toDispatch();// 获取文挡属性

			log.info("打开文档 >>> " + inputFile);
			// 调用Documents对象中Open方法打开文档，并返回打开的文档对象Document
			ppt = Dispatch.call(ppts, "Open", inputFile, true, // ReadOnly
					true, // Untitled指定文件是否有标题
					false// WithWindow指定文件是否可见
			).toDispatch();

			log.info("转换文档 [" + inputFile + "] >>> [" + pdfFile + "]");
			Dispatch.call(ppt, "SaveAs", pdfFile, ppFormatPDF);

			long end = System.currentTimeMillis();
			log.info("用时：" + (end - start) + "ms.");
		} catch (Exception e) {
			log.error("文档转换失败：" + e);
			throw new Exception("文档转换失败："+e.getMessage());
		} finally {
			Dispatch.call(ppt, "Close");
			log.info("关闭文档");
			if (app != null)
				app.invoke("Quit", new Variant[] {});
			ComThread.Release();
			ComThread.quitMainSTA();
		}
		
	}

	/**
	 * (non-Javadoc)
	 * Title: excel2PDF
	 * Description: excel转PDF
	 * Created On: 2018年2月1日
	 * @author Liyewang
	 * <p>
	 * @param inputFile
	 * @param pdfFile
	 * @throws Exception  Office2PDFServiceImpl
	 * @see com.ly.oa.officeservice.service.OfficeConvertService#excel2PDF(java.lang.String, java.lang.String)
	 */
	public void excel2PDF(String inputFile, String pdfFile) throws Exception {
		ComThread.InitSTA();
		long start = System.currentTimeMillis();
		ActiveXComponent app = null;
		Dispatch excel = null;
		try {
			app = new ActiveXComponent("Excel.Application");// 创建一个PPT对象
			app.setProperty("Visible", new Variant(false)); // 不可见打开
			// app.setProperty("AutomationSecurity", new Variant(3)); // 禁用宏
			Dispatch excels = app.getProperty("Workbooks").toDispatch();// 获取文挡属性

			log.info("打开文档 >>> " + inputFile);
			// 调用Documents对象中Open方法打开文档，并返回打开的文档对象Document
			excel = Dispatch.call(excels, "Open", inputFile, false, true).toDispatch();
			// 调用Document对象方法，将文档保存为pdf格式
			log.info("转换文档 [" + inputFile + "] >>> [" + pdfFile + "]");
			// Excel 不能调用SaveAs方法
			Dispatch.call(excel, "ExportAsFixedFormat", xlFormatPDF, pdfFile);
			long end = System.currentTimeMillis();
			log.info("用时：" + (end - start) + "ms.");
		} catch (Exception e) {
			log.error("文档转换失败：" + e);
			throw new Exception("文档转换失败："+e.getMessage());
		} finally {
			Dispatch.call(excel, "Close", false);
			log.info("关闭文档");
			if (app != null)
				app.invoke("Quit", new Variant[] {});
			ComThread.Release();
			ComThread.quitMainSTA();
		}
	}

}
